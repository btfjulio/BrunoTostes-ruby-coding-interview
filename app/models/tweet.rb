class Tweet < ApplicationRecord
  include Paginatable

  belongs_to :user

  scope :by_user, -> (identifier) { where(user: User.by_username(identifier)) if identifier.present? }
  scope :latest, -> { order(created_at: :asc) }
end
