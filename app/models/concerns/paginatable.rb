module Paginatable
  extend ActiveSupport::Concern

  DEFAULT_PAGE = 1
  DEFAULT_PER_PAGE = 5

  included do
    scope :by_page, -> (page) do
      requested_page = page&.to_i || DEFAULT_PAGE
      offset = (requested_page - 1) * DEFAULT_PER_PAGE

      limit(DEFAULT_PER_PAGE).offset(offset)
    end
  end
end