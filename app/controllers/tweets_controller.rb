class TweetsController < ApplicationController
  def index
    tweets = Tweet
                .by_user(params[:user_username])
                .by_page(search_params[:page])
                .latest
    render json: tweets
  end


  def search_params
    params.permit(:page)
  end
end
