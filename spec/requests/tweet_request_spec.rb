require 'rails_helper'

RSpec.describe "Tweets", type: :request do

  RSpec.shared_context 'with multiple tweets' do
    let!(:user_1) { create(:user) }
    let!(:user_2) { create(:user) }

    before do
      5.times do
        create(:tweet, user: user_1, created_at: Date.yesterday)
      end
      5.times do
        create(:tweet, user: user_2, created_at: Date.today)
      end
    end
  end

  describe "#index" do
    let(:result) { JSON.parse(response.body) }

    context 'when fetching tweets by user' do
      include_context 'with multiple tweets'

      it 'returns only the 5 latest tweets' do
        get tweets_path

        latest_tweets = Tweet.all.order(created_at: :asc).limit(5)

        expect(result.size).to eq(latest_tweets.size)
        expect(result.map { |element| element['id'] } ).to match_array(latest_tweets.ids)
      end
    end

    context 'when fetching tweets by page' do
      include_context 'with multiple tweets'

      it 'returns only the specified page tweets' do
        get tweets_path(params: { page: 2 })

        page_tweets = Tweet.all.order(created_at: :desc).limit(5)

        expect(result.size).to eq(page_tweets.size)
        expect(result.map { |element| element['id'] } ).to match_array(page_tweets.ids)
      end
    end

    context 'when fetching tweets by user' do
      include_context 'with multiple tweets'

      it 'returns only specified user tweets' do
        get tweets_path(user_1)

        expect(result.size).to eq(user_1.tweets.size)
        expect(result.map { |element| element['id'] } ).to match_array(user_1.tweets.ids)
      end
    end
  end
end
