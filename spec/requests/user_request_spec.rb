require 'rails_helper'

RSpec.describe "Users", type: :request do

  RSpec.shared_context 'with multiple companies' do
    let!(:company_1) { create(:company) }
    let!(:company_2) { create(:company) }

    before do
      5.times do
        create(:user, company: company_1)
      end
      5.times do
        create(:user, company: company_2)
      end
    end
  end

  describe "#index" do
    let(:result) { JSON.parse(response.body) }

    context 'when fetching users by company' do
      include_context 'with multiple companies'

      it 'returns only the users for the specified company' do
        get company_users_path(company_1)
        
        expect(result.size).to eq(company_1.users.size)
        expect(result.map { |element| element['id'] } ).to eq(company_1.users.ids)
      end
    end

    context 'when fetching all users' do
      include_context 'with multiple companies'

      it 'returns all the users' do
        get(users_path)
        
        expect(result.size).to eq(User.all.size)
        expect(result.pluck('id')).to match_array(User.all.ids)
      end
    end

    context 'when fetching users by name' do
      let!(:company) { create(:company) }
      let(:names) { %w[Max Mathew Dustin John] }
      let!(:user) do
        names.map { |name| create(:user, company: company, username: name) }
      end

      it 'returns only the users with specified characters within the name' do
        get(users_path, params: { username: 'Ma' })
        
        expect(result.size).to eq(2)
        expect(result.map { |element| element['username'] } ).to match_array(%w[Mathew Max])
      end
    end
  end
end
